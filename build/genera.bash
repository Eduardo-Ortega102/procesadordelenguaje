#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
bison -t -d -v ../src/sintactico.y
flex ../src/lexico.l
gcc -c sintactico.tab.c
gcc -c lex.yy.c
gcc -o micalc sintactico.tab.o lex.yy.o
