# Procesador de lenguaje con flex y bison

Este repositorio contiene una mini calculadora hecha con Flex y Bison. Sólo reconoce números enteros positivos y permite las operaciones de suma, resta y multiplicación.

### Cómo usar la calculadora

La calculadora está pensada para ejecutarse en un terminal de Linux, de modo que se necesita tener instalados los siguientes programas:

- compilador gcc
- bison
- flex

Se puede instalar todo mediante el comando:
```sh
$ yum group install "Development Tools"
```

A continuación, descargar el repositorio:
```sh
$ git clone https://bitbucket.org/Eduardo-Ortega102/procesadordelenguaje.git
```

Una vez descargado, ejecutar el script ***genera.bash*** :

```sh
$ chmod u+x genera.bash
$ ./genera.bash
```
Ahora se dispone del fichero ejecutable ***micalc***. Para usar la calculadora, se debe introducir la expresión aritmética por la entrada estándar.

#### Ejemplo 1: desde un fichero de texto

```sh
$ cat ejercicio.txt 
2 + 3 * 4
$ ./micalc < ejercicio.txt
2 + 3 * 4 = 14
```
#### Ejemplo 2: desde la entrada estándar
```sh
$ ./micalc 
2 + 3
2 + 3 = 5
```
