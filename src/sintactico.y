%{
#include<stdio.h>
int yyerror(char* mensaje){ printf(mensaje); }
int yywrap(){}
int yydebug = 0;
%}

%union {
   int entero;
}

%token FINLINEA
%token <entero> ENTERO
%left DIFERENCIA SUMA
%left MULTIPLICACION

%type <entero> e

%%
a : s a | s;
s : e FINLINEA { printf(" = %d \n", $1); } | FINLINEA { /* ignorar */ } ;
e : e DIFERENCIA e { $$ = $1 - $3; }
  | e SUMA e { $$ = $1 + $3; }
  | e MULTIPLICACION e { $$ = $1 * $3; }
  | ENTERO { $$ = $1; }
  ;
%%

int main(int argc, char** argv){
   yyparse();
}

