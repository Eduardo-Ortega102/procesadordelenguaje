%{
#include <stdio.h>
#include "../build/sintactico.tab.h"
%}

%option yylineno

delimitador	[ \t]+
entero		[0-9]+
%s IN_COMMENT

%%

"#*"		BEGIN IN_COMMENT;
"#".*		{ /* ignorar comentario de una linea */ }

<IN_COMMENT>{
"*#"      	BEGIN INITIAL;
[^*\n]+   	{ /* eat comment in chunks */ }
"*"       	{ /* eat the lone star */ }
\n        	yylineno++;
}

{delimitador}	{ ECHO; }
"\n"		{ return FINLINEA; }

"-"		{ ECHO; return DIFERENCIA; }
"+"		{ ECHO; return SUMA; }
"*"		{ ECHO; return MULTIPLICACION; }

{entero}	{ yylval.entero = atoi(yytext); ECHO; return ENTERO; }
.		{ printf("<<"); ECHO; printf(">>"); 
		  yyerror("ERROR: caracter desconocido.");
		}
%%
